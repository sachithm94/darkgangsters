#include<iostream.h>
class Point 
{
	protected:
	int x_coor,y_coor;
	public:
	Point(int a,int b);
};

class Circle:public Point
{
	protected:
	double radius;
	public:
	Circle(int a,int b,int r);
	double area();
};

class Cylinder:public Circle
{
	protected:
	double height;
	public:
	Cylinder(double h,double r,int a,int b);
	double area();
	double volume();
};

Point::Point(int a,int b)
{
	x_coor=a; y_coor=b;
}

Circle::Circle(int a,int b,int r):Point(a,b)
{
	radius=r;
}

double Circle::area()
{
	return (3.14*radius*radius);
}

Cylinder::Cylinder(double h,double r,int a,int b):Circle(a,b,r)
{
	height=h;
}

double Cylinder::area()
{
	return ((2*3.14*radius*radius)+(2*3.14*radius*height));
}

double Cylinder::volume()
{
	return (3.14*radius*radius*height);
}

int main()
{
	Cylinder c(2,2,4,6);
	cout<<"Circle Area   >  "<<c.Circle::area()<<"\n";
	cout<<"Cylinder Area >  "<<c.area()<<"\n";
	cout<<"Circle Area   >  "<<c.volume()<<"\n";
}